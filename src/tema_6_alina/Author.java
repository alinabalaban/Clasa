/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_6_alina;

/**
 *
 * @author Alina
 */
public class Author {
    private String authName;
    private String email;
    public Author(String AN, String EM ){
        authName=AN;
        email=EM;
    }
    
    public void setEmail(String a){
	email=a;
	
	System.out.println(" Email set "+email);
}
     public String getEmail(){
	 System.out.println("Email: "+email);
         return email;
     }
     public String getName(){
	 System.out.println("Author Name: "+authName);
         return authName;
     }
     
    @Override
     public String toString(){
         return  authName+" Email "+email;
     }
}
