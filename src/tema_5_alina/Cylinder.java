/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_5_alina;

/**
 *
 * @author Alina
 */
public class Cylinder {

    public void getVolume(float r, float h) {
        final float Pi = 3.1415926535f;
        float Volume = Pi * (float) Math.pow(r, 2) * h;
        System.out.println("Cylinder Volume is " + Volume);
    }
}
