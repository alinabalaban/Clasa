/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_5_alina;

/**
 *
 * @author Alina
 */
public class Sphere {

    public final float Pi = 3.1415926535f;
    private double raza;

    public void getVolume(float r) {
        float r3 = (float) Math.pow(r, 3) / 3;
        float volume = ((4 * Pi) * r3) / 3;
        System.out.println("Sphere Volume is " + volume + " Raza " + r3);

    }
}
