/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_5_alina;

/**
 *
 * @author Alina
 */
public class FirstApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Rectangle firstRectangle = new Rectangle();
        Rectangle firstRectangle2 = new Rectangle();
        Rectangle firstRectangle3 = new Rectangle();
        Sphere firstSphere = new Sphere();
        Cube firstCube = new Cube();
        Pyramid firstPyramid = new Pyramid();
        Cylinder firstCylinder = new Cylinder();
        Cone firstCone = new Cone();

        firstRectangle.getVolume();
        firstRectangle2.getVolume(2, 3);
        firstRectangle3.getVolume(2, 3, 4);
        firstSphere.getVolume(3);
        firstCube.getVolume(8);
        firstPyramid.getVolume(1.2f, 3.4f);
        firstCylinder.getVolume(3f, 7f);
        firstCone.getVolume(8f, 7f);
    }

}
