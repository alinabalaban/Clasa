/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H8_Alina_Balaban_homework;

/**
 *
 * @author Alina
 */
public class OfficeSpace  {

    int desks;
    String roomType;

    /**
     *
     */
    public OfficeSpace(BuildingFloors B, int D) {
        desks=D;
        roomType="Office Space";
    }
    
        @Override
    public String toString() {
        String Tip="A "+roomType+" with number of desks "+desks+".";
        return Tip;
    }

}
