/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package H8_Alina_Balaban_homework;

/**
 *
 * @author Alina
 */
public class Building {

    private final String Adress ;
    private final String Town ;
    
    public Building(String A, String T){
        Adress=A;
        Town=T;
    }

    public void setAdress(String A) {
        A = Adress;
    }

    public String getAdress() {
        return Adress;
    }

    public void setTown(String T) {
        T = Town;
    }

    public String getTown() {
        return Town;
    }

    @Override
    public String toString() {
        String InfoBuilding = "Building was created, this is the adress " + Adress + " in this town " + Town;
        return InfoBuilding;
    }
}
